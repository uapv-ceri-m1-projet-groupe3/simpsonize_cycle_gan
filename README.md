# Cycle_GAN

## Introduction
This repository uses [](https://gitlab.com/tuanct1997/humanface_dataset_creator) in order to create correct form of dataset to train the CycleGAN. This structure of CycleGAN strictly follow all hyper parameter of this [article](https://arxiv.org/pdf/1703.10593.pdf). Thanks to Mr.Jason Brownlee from [this tutorial](https://machinelearningmastery.com/cyclegan-tutorial-with-keras/) that provide a good tutorial to follow. Indeed , this project is not aim to change hyper parameter but to apply CycleGAN on Simpsonize dataset - which contain a big geometric change. This will be a challenge for CycleGAN. The dataset is provided bellow and freely to use as a Benchmark for geometric chance problem.

## Requirement

+ Keras
+ Simpsonize dataset

## dataset:

This dataset contain humanface set crop focus only the head (not a portrait mode) and a Simpson face. The Simpson face is collected from [kaggle](https://www.kaggle.com/kostastokis/simpsons-faces). Each Simpson and Human set contains 1511 images for both training and testing. Testset contains 150 images and 1361 images for trainset.

Dataset can be download from : [Google Drive](https://drive.google.com/drive/folders/13-ipNQfHbBiF2mElz9HwpNQkwGH1ZSP0?usp=sharing)

## Visualize Dataset

The dataset should look like this : 
+ ![image alt ><](images/dataset.jpg "Dataset") 
---
## Result


After 55000 iterations , here is the result : 

+ ![image alt ><](images/result1.png "Simpsonize human")
---
Also we provide an overview from backward direction - humanize with 3 main type of character from Simpson. We move from weird hair shape to weird nose shape and normal head shape. THe last one has less geometric changes so the humanize perform well on that character. This conclusion should be the same for forward direction since CycleGAN strictly follow the Cycle Consistency concept.

+ ![image alt ><](images/result.png "Humanize Simpson")
---

We tested with outside image which is fully not relavant with training set such as image with multiple human face. We found that the model's generization is good when it still find where is the face to convert and remain all other background as the same ( thanks to identity loss)

+ ![image alt ><](images/multi_face.png "Humanize Simpson")
---
